"use client";
import { useState, useEffect } from 'react';
import styled from 'styled-components';


// Interface pour définir le type de l'état du jeu
interface GameState {
  cards: { letter: string; matched: boolean }[];
  flippedIndices: number[];
  matchedPairs: number;
  message: string;
}

// Définissez une interface pour les props du composant Card
interface CardProps {
  flipped: boolean;
  matched: boolean;
  onClick?: () => void;
}

// Liste de cartes pour le jeu de mémoire
const cardList = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H'];

// Fonction pour mélanger les cartes
const shuffleCards = (cards: string[]) => {
  return [...cards, ...cards].sort(() => Math.random() - 0.5);
};

// Styles pour les composants
const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  padding: 20px;
`;

const CardContainer = styled.div`
  display: grid;
  grid-template-columns: repeat(auto-fill, minmax(80px, 1fr));
  gap: 10px;
  margin-top: 20px;
  max-width: 400px;
  margin-left: auto;
  margin-right: auto;
`;


const Card = styled.div<CardProps>`
  width: 80px;
  height: 120px;
  background-color: ${({ matched, flipped }) => (matched ? '#4caf50' : flipped ? '#ff5252' : '#ddd')};
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 18px;
  cursor: ${({ flipped }) => (flipped ? 'default' : 'pointer')};
  transition: background-color 0.3s;

  @media (min-width: 600px) {
    width: 100px;
    height: 150px;
    font-size: 24px;
  }
`;


// Page principale du jeu
const Game = () => {

  // État initial du jeu
  const [score, setScore] = useState(0);

  const [gameState, setGameState] = useState<GameState>({
    cards: shuffleCards(cardList).map((letter) => ({ letter, matched: false })),
    flippedIndices: [],
    matchedPairs: 0,
    message: 'Retournez deux cartes pour trouver les paires!',
  });

  // Effet pour gérer le comportement des cartes retournées
  useEffect(() => {

    if (gameState.flippedIndices.length === 2) {
    const [index1, index2] = gameState.flippedIndices;

    // Vérifie si les cartes correspondent
    if (gameState.cards[index1].letter === gameState.cards[index2].letter) {
      // Laisse les cartes retournées et désactivez leur interaction
      setGameState({
        ...gameState,
        flippedIndices: [],
        matchedPairs: gameState.matchedPairs + 1,
        message: `Bravo! Vous avez trouvé une paire. Score: ${score + 1}`,
        cards: gameState.cards.map((card, index) => 
          index === index1 || index === index2 ? { ...card, matched: true } : card
        )
      });

      // Vérifie si toutes les paires ont été trouvées
      if (gameState.matchedPairs + 1 === cardList.length) {
        setGameState({
          ...gameState,
          message: `Félicitations! Vous avez gagné! Score final: ${score + 1}. Cliquez pour rejouer.`,
          cards: shuffleCards(cardList).map((letter) => ({ letter, matched: false })),
          flippedIndices: [],
          matchedPairs: 0,
        });
        setScore(0);
      } else {
        setScore(gameState.matchedPairs + 1);
      }
    } else {
      // Retourne les cartes après un court délai
      setTimeout(() => {
        setGameState({
          ...gameState,
          flippedIndices: [],
          message: 'Dommage! Essayez à nouveau.',
        });
      }, 1000);
    }
  }
  }, [gameState, score]);

  // Fonction pour gérer le retournement des cartes
  const handleCardClick = (index: number) => {
    // Vérifie si la carte est déjà retournée ou si deux cartes sont déjà retournées
    if (!gameState.flippedIndices.includes(index) && gameState.flippedIndices.length < 2) {
      setGameState({
        ...gameState,
        flippedIndices: [...gameState.flippedIndices, index],
        message: 'Retournez deux cartes pour trouver les paires!',
      });
    }
  };


  // Fonction pour afficher les cartes
  // const renderCards = () => {
  //   return gameState.cards.map((card, index) => (
  //     <Card
  //       key={index}
  //       onClick={() => handleCardClick(index)}
  //       flipped={gameState.flippedIndices.includes(index)}
  //       matched={false}
  //     >
  //       {/* {gameState.flippedIndices.includes(index) ? card : '?'} */}
  //     </Card>
  //   ));
  // };

  return (
    <Container>
      <h1>{gameState.message}</h1>
      <CardContainer>
        {gameState.cards.map((card, index) => (
          <Card
            key={index}
            onClick={() => handleCardClick(index)}
            flipped={gameState.flippedIndices.includes(index) || (card.matched && gameState.matchedPairs < cardList.length)}
                // matched={gameState.cards[index].matched}
                matched={card.matched}
              >
                {gameState.flippedIndices.includes(index) || card.matched ? card.letter : '?'}
          </Card>))}
      </CardContainer>
    </Container>
  );
};

export default Game;
