'use client';
import styled from 'styled-components';
import Link from 'next/link';

// Styles pour la page d'accueil
const Home = () => (
   <Container>
    <h1>Bienvenue sur notre jeu de mémoire!</h1>
    <p>Cliquez sur le lien ci-dessous pour jouer.</p>
    <Link href="/game">
      <GameLink>Commencer le jeu</GameLink>
    </Link>
  </Container>
);

const Container = styled.main`
  text-align: center;
  margin-top: 50px;
`;

const GameLink = styled.p`
  background-color: #333;
  color: #fff;
  padding: 10px 20px;
  text-decoration: none;
  border-radius: 5px;
  font-weight: bold;
  font-size: 18px;
  cursor: pointer;
`;


export default Home;
